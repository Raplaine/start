//##################
//Add preprocesor #
//#################

var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var sourcemaps = require('gulp-sourcemaps');
var autoprefix = require('gulp-autoprefixer');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var glob = require('gulp-sass-glob');
var pug = require('gulp-pug');


//###########
// Location #
//##########

var path = {
  watch: {
    html: '*.html',
    js: 'js/**/*.js',
    pug: 'pug/**/*.pug',
    sass: 'scss/**/**/*.scss'
  },
  build: {
    html: './',
    css: 'css/'
  }
};


//###############
// Bild Config #
//#############



//Sass bild config
var sassOptions = {
  errLogToConsole: true
    // outputStyle: 'expanded',
    // sourceComments: 'line_comments',
    // includePaths: config.css.includePaths
};

gulp.task('sass', function() {
  gulp.src(path.watch.sass)
    .pipe(glob())
    .pipe(sass(sassOptions).on('error', sass.logError))
    .pipe(autoprefix('last 4 versions', '> 1%', 'ie 9', 'ie 10', { cascade: true }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(path.build.css))
    .pipe(browserSync.reload({ stream: true }))
});

//Browser-sync config
gulp.task('browser-sync', function() {
  browserSync({
    ui: {
      port: 3344
    },
    port: 3343,
    server: {
      baseDir: path.build.html

    },
    notify: true
  });
});


//Pug config
gulp.task('bildHtml', function() {
  return gulp.src(path.watch.pug)
    .pipe(plumber({ errorHandler: notify.onError("Error: <%= error.message %>") }))
    .pipe(pug({
      pretty: true
    }))
    .pipe(gulp.dest(path.build.html))
});

//########################
// The Tasks edit below #
//#######################

//General task for Scss Pug an JS
gulp.task('general-watch', ['sass', 'bildHtml', 'browser-sync'], function() {
  gulp.watch(path.watch.sass, ['sass']);
  gulp.watch(path.watch.pug, ['bildHtml']);
  gulp.watch(path.watch.html, browserSync.reload);
  gulp.watch(path.watch.js, browserSync.reload);

});